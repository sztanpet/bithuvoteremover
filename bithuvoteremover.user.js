// ==UserScript==
// @name          Bithu Vote Remover
// @namespace     tag:sztanpet@gmail.com,2011:bithuvoteremover
// @description   Removes the annoying votes from bithu's main page.
// @include       http://bithumen.be/index.php
// @include       http://bithumen.be/
// @include       http://bithumen.ru/index.php
// @include       http://bithumen.ru/
// ==/UserScript==

// Find the anchor element with the "name" attribute, and "poll" value
// Look at it's parent, it should be an H2 element, remove that
// The nextElementSibling of the H2 should be a table, remove that,
// done.

var links = document.getElementsByTagName('a');
for( var i = 0, j = links.length; i < j; i++ ) {
  
  var link = links[i];
  if ( !link || link.hasAttribute('href') )
    continue;
  
  if ( link.getAttribute('name') != 'poll' )
    continue;
  
  removePoll( link );
  break;
  
}

function removePoll( poll ) {
  
  if ( poll.parentNode.tagName.toLowerCase() == 'h2' )
    var h2 = poll.parentNode;
  else // something wrong, the markup probably changed
    return;
  
  if ( poll.parentNode.nextElementSibling.tagName.toLowerCase() == 'table' )
    var table = poll.parentNode.nextElementSibling;
  else
    return;
  
  var root = poll.parentNode.parentNode;
  root.removeChild( h2 );
  root.removeChild( table );
  
}
